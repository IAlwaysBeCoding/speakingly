from scrapy import Item, Field


class JustdialListingItem(Item):

    source_url = Field()
    business_url = Field()
    city = Field()
    search = Field()
    where = Field()
    latitude = Field()
    longitude = Field()

class JustdialBusinessItem(Item):

    business_name = Field()
    business_url = Field()
    phone_number = Field()
    address = Field()
    pin_code = Field()
    landmark = Field()
