import sys

from scrapy.selector import Selector
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose


def create_loader(item_instance, selector_instance, fields,**kwargs):
    default_input_processor = kwargs.get('default_input_processor', None)
    default_output_processor = kwargs.get('default_output_processor', None)

    loader = ItemLoader(item_instance, selector_instance)
    if default_input_processor:
        loader.default_input_processor = default_input_processor
    if default_output_processor:
        loader.default_output_processor = default_output_processor

    for field in fields:
        if isinstance(field, (list, tuple)):
            if len(field) == 4:

                field_type = field[0]
                field_name = field[1]
                field_value = field[2]
                field_processors = field[3]

            elif len(field) == 3:

                field_type = field[0]
                field_name = field[1]
                field_value = field[2]
                field_processors = []


        else:
            raise ValueError(
                'Field needs to be a tuple with 3 or 4 items. ' \
                '1st item is type = (xpath, css, value)' \
                '2nd item is name = name of the field' \
                '3rd item is value = the value of the field' \
                '4rd tiem is optional and is processors = a ' \
                'list of processors to apply to the output'
            )

        if field_type == 'xpath':
            loader.add_xpath(field_name, field_value, *field_processors)
        elif field_type == 'css':
            loader.add_css(field_name, field_value, *field_processors)
        elif field_type == 'value':
            loader.add_value(field_name, field_value, *field_processors)

    return loader

def default_missing_keys(
        item, default_value, except_keys=[],
        missing_key_defaults={}):

    missing_keys = list(set(item.fields.keys()) - set(item.keys()))
    for missing_key in missing_keys:
        if except_keys:
            if missing_key not in except_keys:
                item[missing_key] = default_value
        else:
            item[missing_key] = default_value

        if missing_key in missing_key_defaults:
            item[missing_key] = missing_key_defaults[missing_key]
def strip_spaces(raw_str):

    str_types = (str) if sys.version_info[0] == 3 else (unicode, str)

    if isinstance(raw_str,  str_types):
        return raw_str.strip().replace('\n', ' ').replace('\t', '')
    else:
        return raw_str

class PaginateMixin(object):

    def paginate(self,response):

        selector = Selector(response)
        meta = response.meta
        next_text = meta.pop('next_text',None)
        base_domain = meta.pop('base_domain',None)
        links_xpath = meta.pop('links_xpath',None)
        referer = meta.pop('referer',None)
        callback = meta.pop('callback',None)

        if not links_xpath:
            return

        pages_link_xpath, pages_text_xpath = self.build_link_xpaths(xpath=links_xpath)

        get_pages_link = selector.xpath(pages_link_xpath).extract() if pages_link_xpath else []
        get_pages_text = selector.xpath(pages_text_xpath).extract() if pages_text_xpath else []

        if not get_pages_link:
            return

        next_page_link = None

        if get_pages_link:
            if next_text:
                for i, get_page_text in enumerate(get_pages_text):
                    if get_page_text == next_text:
                        next_page_link = get_pages_link[i]
                        break

            else:
                if get_pages_link:
                    next_page_link = get_pages_link[0]

            if next_page_link:
                if not next_page_link.startswith('http://') and not next_page_link.startswith('https://'):

                    next_page_link = '{}{}'.format(base_domain,next_page_link)

                meta.update({'page_link':next_page_link})
                if referer:
                    return Request(url=next_page_link,
                                   callback=callback,
                                   meta=meta,
                                   headers={'Referer':referer})
                else:
                    return Request(url=next_page_link,
                                   meta=meta,
                                   callback=callback)

    def build_link_xpaths(self,xpath):
        text_xpath = '{}/text()'.format(xpath)
        href_xpath = '{}/@href'.format(xpath)

        return href_xpath, text_xpath

class NoneEmpty(object):
    def __call__(self,values):
        if values is None:
            return ''
        elif isinstance(values, (list, tuple)):
            if len(values) == 1 and values[0] == '':
                return ''
        return TakeFirst()(values)
class Replace(object):
    def __init__(self, find_str, rep_str):
        self.find_str = find_str
        self.rep_str = rep_str

    def __call__(self, value):
        return value.replace(self.find_str, self.rep_str)

class Prefix(object):
    def __init__(self, prefix_str):
        self.prefix_str = prefix_str

    def __call__(self, value):
        return '{}{}'.format(self.prefix_str, value)

class Suffix(object):
    def __init__(self,suffix_str):
        self.suffix_str = suffix_str

    def __call__(self, value):
        return '{}{}'.format(value, self.suffix_str)

class TakeN(object):
    def __init__(self, index, default=''):
        self.index = index
        self.default = default

    def __call__(self, values):
        try:
            return values[self.index]
        except:
            return self.default

class SplitTakeIndex(object):
    def __init__(self, split_str, index, default=''):
        self.split_str = split_str
        self.index = index
        self.default = default

    def __call__(self, values):
        try:
            return values.split(self.split_str)[self.index]
        except:
            return self.default

class TakeOnlyIndexes(object):
    def __init__(self, indexes):

        self.indexes = indexes

    def __call__(self, values):

        filtered_values = []

        for i, value in enumerate(values):
            if i in self.indexes:
                filtered_values.append(value)

        return filtered_values
