
import json
import os
from uuid import uuid4


from furl import furl
from scrapy import Spider, Selector, Request
from scrapy.exceptions import  CloseSpider
from scrapy.loader.processors import Identity, TakeFirst, MapCompose, SelectJmes
from six.moves.urllib.parse import urljoin

from tinycss2 import parse_stylesheet

from speakingly.items import JustdialListingItem, JustdialBusinessItem
from speakingly.utils import default_missing_keys, strip_spaces, NoneEmpty, TakeN

class JustdialException(Exception):
    pass

class JustdialSpider(Spider):
    name = 'justdial'
    allowed_domains = ['justdial.com']
    start_urls = ['http://justdial.com/']

    LISTING_MODE = '1'
    BUSINESS_MODE = '2'

    default_city = 'Mumbai'
    default_what = 'Gyms'
    default_where = 'Mhb Colony-Borivali West'
    default_distance = 5

    def __init__(self, *args, **kwargs):

        self.input = kwargs.get('input', None)
        self.mode = kwargs.get('mode', self.LISTING_MODE)
        self.page = kwargs.get('page', 1)
        self.distance = kwargs.get('distance', self.default_distance)
        self._verify_input_file(self.input)
        self._verify_mode(self.mode)

    def _verify_input_file(self, input_file):

        if input_file:
            file_exists = os.path.exists(input_file)

            if not file_exists:
                raise JustdialException(
                    'Input file is required please pass an input file ex: -a input=FILE')

    def _verify_mode(self, mode):

        if mode not in (self.LISTING_MODE, self.BUSINESS_MODE):
            raise JustDialException(
                'Invalid mode only these modes are valid:{}'.format((self.LISTING_MODE, self.BUSINESS_MODE)))

    def _parse_starting_url(self, url):
        f = furl(url)
        segments = f._path.segments

        if len(segments) != 2:
            raise JustdialException(
                'Invalid url. Example of valid url:{}'.format(
                    'https://www.justdial.com/Mumbai/Gyms-in-mhb-colony-borivali-west')
            )

        city = segments[0]
        what = segments[1].split('-in')[0].replace('-', ' ')
        where = segments[1].split('-in-')[1].replace('-', ' ')

        return city, what, where

    def _extract_whatsapp_css_rules(self, response):

        raw_css = response.xpath('//style[@type="text/css"][2]/text()').extract_first()
        classes = {
            '"\U0009d001"' : '0',
            '"\U0009d002"' : '1',
            '"\U0009d003"' : '2',
            '"\U0009d004"' : '3',
            '"\U0009d005"' : '4',
            '"\U0009d006"' : '5',
            '"\U0009d007"' : '6',
            '"\U0009d008"' : '7',
            '"\U0009d009"' : '8',
            '"\U0009d010"' : '9',
            '"\U0009d011"' : '+',
            '"\U0009d013"' : ')',
            '"\U0009d014"' : '(',

        }
        parsed_chars = {}
        if raw_css:
            rules = parse_stylesheet(raw_css)

            for i, r in enumerate(rules):

                if r.type == 'qualified-rule':
                    content = r.content[2].serialize()

                    if content in classes:
                        class_found = r.prelude[1].serialize()
                        parsed_chars[class_found] = classes[content]


        return parsed_chars

    def start_requests(self):

        if self.mode == self.LISTING_MODE:
            parsing_func = self.parse_category_page
        elif self.mode == self.BUSINESS_MODE:
            parsing_func = self.parse_business_page


        with open(self.input, 'r') as f:

            for starting_url in f:
                starting_url = starting_url.strip()
                self.log(starting_url)

                if self.mode == self.LISTING_MODE:
                    city, what, where = self._parse_starting_url(starting_url)
                    self
                elif self.mode == self.BUSINESS_MODE:
                    city = self.default_city
                    what = self.default_what
                    where = self.default_where


                yield Request(starting_url,
                            cookies={
                                'dealBackCity' : city,
                                'inweb_city' : city,
                                'inweb_what' : what,
                                'main_city' : city,
                                'scity' : city,
                                'usrcity' : city,
                                'tab': 'distance{}'.format(self.distance),
                                'view' : 'lst_v'
                            },
                            meta={
                                'city' : city,
                                'what' : what,
                                'where' : where,
                                'has_distance_set' : False,
                                'source_url' : starting_url,
                                'distance' : 'distance{}'.format(self.distance)
                            },
                            callback=parsing_func)

    def parse_category_page(self, response):

        city = response.meta['city']
        what = response.meta['what']
        where = response.meta['where']

        if not response.meta.get('has_distance_set', True):
            catid = response.xpath('//input[@id="ncatid1"]/@value').extract_first()
            query = {
                "areaidnt" : "",
                "city" : city,
                "what" : what,
                "where" : where,
                "catid" : catid,
                "type" : "1",
                "group" : "",
                "itab" : "1",
                "restflt" : "0",
                "classic" : "",
                "asflg" : "",
                "enflg" : "1",
                "prid" : "",
                "ncatid1" : catid,
                "psearch" : "",
                "sfsearch" : "",
                "reshref" : "",
                "jdg" : "",
                "token" : "",
                "jdsrc" : "",
            }
            f = furl('https://www.justdial.com/router_map')
            url = f.set(query).url

            yield Request(url,
                          meta={
                              'city' : city,
                              'what' : what,
                              'where' : where,
                              'has_distance_set' : True
                          },
                          cookies={
                                'dealBackCity' : city,
                                'inweb_city' : city,
                                'inweb_what' : what,
                                'main_city' : city,
                                'scity' : city,
                                'usrcity' : city,
                                'tab': 'distance{}'.format(self.distance),
                                'view' : 'lst_v',
                                'sarea' : where,
                              'catid' : catid,
                              'prevcatid' : catid,
                              'profbd' : '0'
                            },

                          callback=self.parse_category_page)

        else:
            business_urls = self.parse_business_listing_urls(response)

            for business_url in business_urls:
                url, city, search, where = business_url

                category_item = JustdialListingItem()

                category_item['source_url'] = response.url
                category_item['business_url'] = url
                category_item['city'] = city
                category_item['search'] = search
                category_item['where'] = where
                category_item['latitude'] = response.xpath('//input[@id="median_latitude"]/@value').extract_first()
                category_item['longitude'] = response.xpath('//input[@id="median_longitude"]/@value').extract_first()

                yield category_item

            #for req in self.build_business_listing_requests(response):
            #    yield req

            #yield self.next_page(response)

    def build_business_listing_requests(self, response):

        urls = response.xpath('//span[@class="jcn"]/a/@href').extract()

        for url in urls:

            yield Request(
                url,
                meta={
                    'city' : response.meta['city'],
                    'what' : response.meta['what'],
                    'where' : response.meta['where']
                },
                callback=self.parse_business_page
            )

        yield

    def parse_business_listing_urls(self, response):

        urls = response.xpath('//span[@class="jcn"]/a/@href').extract()

        for url in urls:

            yield (url,
                   response.meta['city'],
                   response.meta['what'],
                   response.meta['where'])

    def parse_business_page(self, response):

        address, pin_code, landmark = self.parse_address(response)

        item = JustdialBusinessItem()

        item['business_name'] = response.xpath('//span[@class="fn"]/text()').extract_first()
        item['business_url'] = response.url
        item['phone_number'] = self.parse_whatsapp_contact_number(response)
        item['address'] = address
        item['pin_code'] = pin_code
        item['landmark'] = landmark
        item['user_id'] = self.parse_user_id(response)


        yield item

    def parse_user_id(self, response):

        contact_button = response.xpath('//button[@class="jbtn fltrt"]/@onclick').extract()

        if len(contact_button) > 1:
            parts = contact_button[1].split("','")

            if len(parts) > 2:
                f = furl(parts[1])
                user_id = f.args.get('userid','')
                return user_id

        return ''

    def parse_address(self, response):

        full_address = response.xpath(
            '//span[@id="fulladdress"]//span[@class="lng_add"]/text()').extract_first()

        address = ''
        pin_code = ''
        landmark = ''

        if full_address:
            address = full_address.split(' - ')[0]
            pin_code = full_address.split(' - ')[1].split(',')[0]
            landmark = full_address.split(pin_code)[1][2:]

        return address, pin_code, landmark

    def parse_whatsapp_contact_number(self, response):

        classes = self._extract_whatsapp_css_rules(response)
        numbers = set()

        contact_links = response.xpath('//a[@class="tel ttel" or @class="tel mtel"]').extract()

        if contact_links:
            for contact_link in contact_links:
                chars = []
                encoded_chars = Selector(text=contact_link) \
                    .xpath('//span[contains(@class, "mobilesv")]/@class') \
                    .extract()

                for encoded_char in encoded_chars:
                    class_name = encoded_char.replace('mobilesv ','')

                    decoded_char = classes[class_name]
                    chars.append(decoded_char)

                numbers.add("".join(chars))

        return ",".join(list(numbers))

    def next_page(self, response):
        page = response.meta.get('page', self.page)
        next_page_link = response.xpath("//a[@rel='next']/@href").extract_first()

        if next_page_link:
            return Request(next_page_link,
                          meta={
                              'page' : page + 1,
                              'city' : response.meta['city'],
                              'what' : response.meta['what'],
                              'where' : response.meta['where']
                          },
                          callback=self.parse_category_page)

    def error_parse_category_ajax_page(self, failure):
        pass

